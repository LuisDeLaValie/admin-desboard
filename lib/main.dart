import 'package:admin_dasboard/api/cafeApi.dart';
import 'package:admin_dasboard/providers/auth_provider.dart';
import 'package:admin_dasboard/providers/categorias_provider.dart';
import 'package:admin_dasboard/providers/sidemenu_provider.dart';
import 'package:admin_dasboard/router/route.dart';
import 'package:admin_dasboard/services/loaclStorach.dart';
import 'package:admin_dasboard/services/navigarions_servises.dart';
import 'package:admin_dasboard/services/notification_services.dart';
import 'package:admin_dasboard/ui/layouts/auth/auth_layout.dart';
import 'package:admin_dasboard/ui/layouts/dashboard/dashboard_layout.dart';
import 'package:admin_dasboard/ui/layouts/splah_layout.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  await LocalStorage.configurePres();

  Flurorouter.configureRouter();
  CafeApi.configureDio();

  runApp(AppState());
}

class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(lazy: false, create: (_) => AuthProvider()),
        ChangeNotifierProvider(lazy: false, create: (_) => SideMenuProvider()),
        ChangeNotifierProvider(create: (_) => CategoriasProvider()),
      ],
      child: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Admin dashboard',
      initialRoute: '/',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: Flurorouter.router.generator,
      navigatorKey: NavigationServices.navigatorKy,
      scaffoldMessengerKey: NotificationServices.messagenrKey,
      builder: (context, child) {
        final pro = Provider.of<AuthProvider>(context);

        if (pro.status == AuthSatate.cehking) return SpalsLayout();

        if (pro.status == AuthSatate.authenticated) {
          return DashboardLayout(child: child!);
        } else {
          return AuthLayout(child: child!);
        }
      },
      theme: ThemeData.light().copyWith(
        scrollbarTheme: ScrollbarThemeData().copyWith(
          thumbColor: MaterialStateProperty.all(Colors.white),
        ),
      ),
    );
  }
}
