import 'package:flutter/material.dart';

class UsersDataSorse extends DataTableSource {
  final BuildContext context;

  UsersDataSorse(this.context);

  @override
  DataRow getRow(int index) {
    return DataRow.byIndex(index: index, cells: [
      DataCell(Text('Avatr $index')),
      DataCell(Text('Nombre $index')),
      DataCell(Text('Email $index')),
      DataCell(Text('UID $index')),
      DataCell(Text('Acciones $index')),
    ]);

    // return DataRow.byIndex(index: index, cells: [
    //   DataCell(Text(categoria.id)),
    //   DataCell(Text(categoria.nombre)),
    //   DataCell(Text(categoria.usuario.nombre)),
    //   DataCell(Row(
    //     children: [
    //       IconButton(
    //           icon: Icon(Icons.edit_outlined),
    //           onPressed: () {
    //             showModalBottomSheet(
    //               backgroundColor: Colors.transparent,
    //               context: context,
    //               builder: (_) => CategorisModal(categoria: categoria),
    //             );
    //           }),
    //       IconButton(
    //           icon: Icon(Icons.delete_outline,
    //               color: Colors.red.withOpacity(0.8)),
    //           onPressed: () {
    //             final dialog = AlertDialog(
    //               title: Text('¿Está seguro de borrarlo?'),
    //               content: Text('¿Borrar definitivamente ${categoria.nombre}?'),
    //               actions: [
    //                 TextButton(
    //                   child: Text('No'),
    //                   onPressed: () {
    //                     Navigator.of(context).pop();
    //                   },
    //                 ),
    //                 TextButton(
    //                   child: Text('Si, borrar'),
    //                   onPressed: () async {
    //                     await Provider.of<CategoriasProvider>(context,
    //                             listen: false)
    //                         .deletCAtegoria(categoria.id);

    //                     Navigator.of(context).pop();
    //                   },
    //                 ),
    //               ],
    //             );

    //             showDialog(context: context, builder: (_) => dialog);
    //           }),
    //     ],
    //   )),
    // ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => 100;

  @override
  int get selectedRowCount => 0;
}
