import 'dart:convert';

class Categoria {
  final String id;
  final String nombre;
  final _Usuario usuario;
  Categoria({
    required this.id,
    required this.nombre,
    required this.usuario,
  });

  Categoria copyWith({
    String? id,
    String? nombre,
    _Usuario? usuario,
  }) {
    return Categoria(
      id: id ?? this.id,
      nombre: nombre ?? this.nombre,
      usuario: usuario ?? this.usuario,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'nombre': nombre,
      'usuario': usuario.toMap(),
    };
  }

  factory Categoria.fromMap(Map<String, dynamic> map) {
    return Categoria(
      id: map['_id'],
      nombre: map['nombre'],
      usuario: _Usuario.fromMap(map['usuario']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Categoria.fromJson(String source) =>
      Categoria.fromMap(json.decode(source));

  @override
  String toString() =>
      'Categoria(_id: $id, nombre: $nombre, usuario: $usuario)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Categoria &&
        other.id == id &&
        other.nombre == nombre &&
        other.usuario == usuario;
  }

  @override
  int get hashCode => id.hashCode ^ nombre.hashCode ^ usuario.hashCode;
}

class _Usuario {
  final String id;
  final String nombre;
  _Usuario({
    required this.id,
    required this.nombre,
  });

  _Usuario copyWith({
    String? id,
    String? nombre,
  }) {
    return _Usuario(
      id: id ?? this.id,
      nombre: nombre ?? this.nombre,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'nombre': nombre,
    };
  }

  factory _Usuario.fromMap(Map<String, dynamic> map) {
    return _Usuario(
      id: map['_id'],
      nombre: map['nombre'],
    );
  }

  String toJson() => json.encode(toMap());

  factory _Usuario.fromJson(String source) =>
      _Usuario.fromMap(json.decode(source));

  @override
  String toString() => 'Usuario(_id: $id, nombre: $nombre)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is _Usuario && other.id == id && other.nombre == nombre;
  }

  @override
  int get hashCode => id.hashCode ^ nombre.hashCode;
}
