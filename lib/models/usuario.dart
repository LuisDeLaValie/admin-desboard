
import 'dart:convert';

class Usuario {
  final String rol;
  final bool estado;
  final bool google;
  final String nombre;
  final String correo;
  final String uid;
  Usuario({
    required this.rol,
    required this.estado,
    required this.google,
    required this.nombre,
    required this.correo,
    required this.uid,
  });

  Usuario copyWith({
    String? rol,
    bool? estado,
    bool? google,
    String? nombre,
    String? correo,
    String? uid,
  }) {
    return Usuario(
      rol: rol ?? this.rol,
      estado: estado ?? this.estado,
      google: google ?? this.google,
      nombre: nombre ?? this.nombre,
      correo: correo ?? this.correo,
      uid: uid ?? this.uid,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'rol': rol,
      'estado': estado,
      'google': google,
      'nombre': nombre,
      'correo': correo,
      'uid': uid,
    };
  }

  factory Usuario.fromMap(Map<String, dynamic> map) {
    return Usuario(
      rol: map['rol'],
      estado: map['estado'],
      google: map['google'],
      nombre: map['nombre'],
      correo: map['correo'],
      uid: map['uid'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Usuario.fromJson(String source) => Usuario.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Usuario(rol: $rol, estado: $estado, google: $google, nombre: $nombre, correo: $correo, uid: $uid)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Usuario &&
      other.rol == rol &&
      other.estado == estado &&
      other.google == google &&
      other.nombre == nombre &&
      other.correo == correo &&
      other.uid == uid;
  }

  @override
  int get hashCode {
    return rol.hashCode ^
      estado.hashCode ^
      google.hashCode ^
      nombre.hashCode ^
      correo.hashCode ^
      uid.hashCode;
  }
}



