import 'dart:convert';

import 'package:admin_dasboard/models/usuario.dart';

class UsersResponse {
  final int total;
  final List<Usuario> usuarios;
  UsersResponse({
    required this.total,
    required this.usuarios,
  });

  UsersResponse copyWith({
    int? total,
    List<Usuario>? usuarios,
  }) {
    return UsersResponse(
      total: total ?? this.total,
      usuarios: usuarios ?? this.usuarios,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'usuarios': usuarios.map((x) => x.toMap()).toList(),
    };
  }

  factory UsersResponse.fromMap(Map<String, dynamic> map) {
    return UsersResponse(
      total: map['total']?.toInt(),
      usuarios: List<Usuario>.from(map['usuarios']?.map((x) => Usuario.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory UsersResponse.fromJson(String source) => UsersResponse.fromMap(json.decode(source));

  @override
  String toString() => 'UsersResponse(total: $total, usuarios: $usuarios)';

  

}
