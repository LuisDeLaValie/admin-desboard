import 'dart:convert';

import 'package:admin_dasboard/models/usuario.dart';

class AuthResponse {
  final Usuario usuario;
  final String token;
  AuthResponse({
    required this.usuario,
    required this.token,
  });

  AuthResponse copyWith({
    Usuario? usuario,
    String? token,
  }) {
    return AuthResponse(
      usuario: usuario ?? this.usuario,
      token: token ?? this.token,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'usuario': usuario.toMap(),
      'token': token,
    };
  }

  factory AuthResponse.fromMap(Map<String, dynamic> map) {
    return AuthResponse(
      usuario: Usuario.fromMap(map['usuario']),
      token: map['token'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AuthResponse.fromJson(String source) => AuthResponse.fromMap(json.decode(source));

  @override
  String toString() => 'AuthResponse(usuario: $usuario, token: $token)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is AuthResponse &&
      other.usuario == usuario &&
      other.token == token;
  }

  @override
  int get hashCode => usuario.hashCode ^ token.hashCode;
}
