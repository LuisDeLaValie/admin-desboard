import 'dart:convert';

import '../categorias.dart';

class CategorisResponse {
  final int total;
  final List<Categoria> categorias;
  CategorisResponse({
    required this.total,
    required this.categorias,
  });

  CategorisResponse copyWith({
    int? total,
    List<Categoria>? categorias,
  }) {
    return CategorisResponse(
      total: total ?? this.total,
      categorias: categorias ?? this.categorias,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'categorias': categorias.map((x) => x.toMap()).toList(),
    };
  }

  factory CategorisResponse.fromMap(Map<String, dynamic> map) {
    return CategorisResponse(
      total: map['total']?.toInt(),
      categorias: List<Categoria>.from(map['categorias']?.map((x) => Categoria.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory CategorisResponse.fromJson(String source) => CategorisResponse.fromMap(json.decode(source));

  @override
  String toString() => 'CategorisResponse(total: $total, categorias: $categorias)';

 

}


