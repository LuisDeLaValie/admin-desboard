import 'package:admin_dasboard/providers/auth_provider.dart';
import 'package:admin_dasboard/ui/view/dashboar_view.dart';
import 'package:admin_dasboard/ui/view/login_viw.dart';
import 'package:admin_dasboard/ui/view/register_viw.dart';
import 'package:fluro/fluro.dart';
import 'package:provider/provider.dart';

class AdminHandlers {
  static Handler login = new Handler(handlerFunc: (context, params) {
    final pro = Provider.of<AuthProvider>(context!, listen: false);

    if (pro.status == AuthSatate.authenticated) {
      return DashboartView();
    } else {
      return LoginView();
    }
  });
  static Handler reguister = new Handler(handlerFunc: (context, params) {
    final pro = Provider.of<AuthProvider>(context!, listen: false);
    if (pro.status == AuthSatate.authenticated) {
      return DashboartView();
    } else {
      return ReguisterView();
    }
  });
}
