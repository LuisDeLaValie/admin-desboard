import 'package:admin_dasboard/router/admin_handles.dart';
import 'package:admin_dasboard/router/no_page_fount_handlers.dart.dart';
import 'package:fluro/fluro.dart';

import 'dashboar_handres.dart';

class Flurorouter {
  static final FluroRouter router = new FluroRouter();

  static String rootRoute = '/';
  //auth router
  static String loginRoute = '/auth/login';
  static String registerRoute = '/auth/register';

  // Dashboard
  static String dashboardRoute = '/dashboard';
  static String iconsRoute = '/dashboard/icons';  
  static String categoriasRoute = '/dashboard/categorias';  
  static String userRoute = '/dashboard/users';  

  static void configureRouter() {
    //auth router
    router.define(loginRoute,
        handler: AdminHandlers.login, transitionType: TransitionType.none);
    router.define(registerRoute,
        handler: AdminHandlers.reguister, transitionType: TransitionType.none);
    router.define(rootRoute,
        handler: AdminHandlers.login, transitionType: TransitionType.none);

    // desboard
    router.define(dashboardRoute, handler: DashboarHandlers.dashboar);
    router.define(iconsRoute, handler: DashboarHandlers.icons);
    router.define(categoriasRoute, handler: DashboarHandlers.categorias);
    router.define(userRoute, handler: DashboarHandlers.users);

    router.notFoundHandler = NotPageHandlers.noPageFound;
  }
}
