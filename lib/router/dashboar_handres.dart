import 'package:admin_dasboard/providers/sidemenu_provider.dart';
import 'package:admin_dasboard/router/route.dart';
import 'package:admin_dasboard/ui/view/categores_view.dart';
import 'package:admin_dasboard/ui/view/dashboar_view.dart';
import 'package:admin_dasboard/ui/view/icons_view.dart';
import 'package:admin_dasboard/ui/view/users_view.dart';
import 'package:fluro/fluro.dart';
import 'package:provider/provider.dart';

class DashboarHandlers {
  static Handler dashboar = new Handler(handlerFunc: (context, params) {
    final pro = Provider.of<SideMenuProvider>(context!, listen: false);
    pro.setcurrentPage(Flurorouter.dashboardRoute);

    return DashboartView();
  });
  static Handler icons = new Handler(handlerFunc: (context, params) {
    final pro = Provider.of<SideMenuProvider>(context!, listen: false);
    pro.setcurrentPage(Flurorouter.iconsRoute);

    return IconsView();
  });

  static Handler categorias = new Handler(handlerFunc: (context, params) {
    final pro = Provider.of<SideMenuProvider>(context!, listen: false);
    pro.setcurrentPage(Flurorouter.categoriasRoute);

    return CategoriView();
  });
  static Handler users = new Handler(handlerFunc: (context, params) {
    final pro = Provider.of<SideMenuProvider>(context!, listen: false);
    pro.setcurrentPage(Flurorouter.userRoute);

    return UsersView();
  });
}
