import 'package:admin_dasboard/providers/sidemenu_provider.dart';

import 'package:admin_dasboard/ui/view/no_page_found_view.dart';
import 'package:fluro/fluro.dart';
import 'package:provider/provider.dart';

class NotPageHandlers {
  static Handler noPageFound = new Handler(handlerFunc: (context, params) {
    final pro = Provider.of<SideMenuProvider>(context!, listen: false);
    pro.setcurrentPage('sabra');
    return NoPageFoundView();
  });
}
