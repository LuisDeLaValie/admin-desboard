
import 'package:admin_dasboard/services/loaclStorach.dart';
import 'package:dio/dio.dart';

class CafeApi {
  static Dio _dio = new Dio();

  static void configureDio() {
    // Base del url
    _dio.options.baseUrl = "http://localhost:8081";

    // Configurar Headers
    _dio.options.headers = {
      'x-token': LocalStorage.pref.getString('token') ?? ''
    };
  }

  static Future httpGet(String path) async {
    try {
      final resp = await _dio.get(path);

      return resp.data;
    } catch (e) {
      print(e);
      throw ('Error en el Get');
    }
  }
  static Future httpDel(String path) async {
    try {
      final resp = await _dio.delete(path);

      return resp.data;
    } catch (e) {
      print(e);
      throw ('Error en el Get');
    }
  }

  static Future httpPost(String path, Map<String, dynamic> data) async {
    
    final formData = FormData.fromMap(data);

    try {
      final resp = await _dio.post(path,data: formData);

      return resp.data;
    } catch (e) {
      print(e);
      throw ('Error en el Post');
    }
  }
  static Future httpPut(String path, Map<String, dynamic> data) async {
    
    final formData = FormData.fromMap(data);

    try {
      final resp = await _dio.put(path,data: formData);

      return resp.data;
    } catch (e) {
      print(e);
      throw ('Error en el Post');
    }
  }
}
