import 'package:flutter/material.dart';

class NavigationServices {
  static final navigatorKy = new GlobalKey<NavigatorState>();

  static Future<dynamic> navigatorTo(String routeName) {
    return navigatorKy.currentState!.pushNamed(routeName);
  }

  static Future<dynamic> replaceTo(String routeName) {
    return navigatorKy.currentState!.pushReplacementNamed(routeName);
  }

  static back() {
    navigatorKy.currentState!.pop();
  }
}
