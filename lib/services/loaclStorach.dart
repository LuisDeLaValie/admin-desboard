import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  static late SharedPreferences pref;

  static Future<void> configurePres() async {
    pref = await SharedPreferences.getInstance();
  }
}
