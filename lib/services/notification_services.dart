import 'package:flutter/material.dart';

class NotificationServices {
  static GlobalKey<ScaffoldMessengerState> messagenrKey =
      GlobalKey<ScaffoldMessengerState>();

  static showSnackBarErro(String ms) {
    final snack = SnackBar(
      backgroundColor: Colors.red.withOpacity(0.9),
      content: Text(
        ms,
        style: TextStyle(color: Colors.white),
      ),
    );
    messagenrKey.currentState!.showSnackBar(snack);
  }

  static showSnackBar(String ms) {
    final snack = SnackBar(
      content: Text(
        ms,
        style: TextStyle(color: Colors.white),
      ),
    );
    messagenrKey.currentState!.showSnackBar(snack);
  }
}
