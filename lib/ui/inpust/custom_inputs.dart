import 'package:flutter/material.dart';

class CustomInputs {
  static InputDecoration loginInputDecorations({
    required String hint,
    required String label,
    required IconData icon,
  }) {
    return InputDecoration(
      border: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white.withOpacity(0.3)),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white.withOpacity(0.3)),
      ),
      hintText: hint,
      labelText: label,
      suffixIcon: Icon(icon, color: Colors.grey),
      prefixStyle: TextStyle(color: Colors.grey),
      labelStyle: TextStyle(color: Colors.grey),
      hintStyle: TextStyle(color: Colors.grey),
    );
  }
  static InputDecoration searchInputDecorations({
    required String hint,
    // required String label,
    required IconData icon,
  }) {
    return InputDecoration(
      border: InputBorder.none,
      // enabledBorder: OutlineInputBorder(
      //   borderSide: BorderSide(color: Colors.white.withOpacity(0.3)),
      // ),
      hintText: hint,
      // labelText: label,
      prefixIcon: Icon(icon, color: Colors.grey),
      prefixStyle: TextStyle(color: Colors.grey),
      labelStyle: TextStyle(color: Colors.grey),
      hintStyle: TextStyle(color: Colors.grey),
    );
  }
}
