
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WhileCard extends StatelessWidget {
  final String? title;
  final Widget child;
  final double? width;
  const WhileCard({
    Key? key,
    this.title,
    required this.child,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width,
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
            )
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (this.title != null) ...[
            FittedBox(
              child: Text(
                this.title!,
                style: GoogleFonts.roboto(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Divider(),
          ],
          this.child
        ],
      ),
    );
  }
}
