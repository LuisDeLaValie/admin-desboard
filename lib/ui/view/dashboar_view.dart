import 'package:admin_dasboard/providers/auth_provider.dart';
import 'package:admin_dasboard/ui/cards/while_card.dart';
import 'package:admin_dasboard/ui/labels/custom_labels.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DashboartView extends StatelessWidget {
  const DashboartView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pro = Provider.of<AuthProvider>(context);

    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Dashboard view',
              style: CustomLabels.h1,
            ),
            SizedBox(height: 10),
            WhileCard(
              title: pro.user?.nombre ?? '',
              child: Text(pro.user?.correo ?? ''),
            )
          ],
        ),
      ),
    );
  }
}
