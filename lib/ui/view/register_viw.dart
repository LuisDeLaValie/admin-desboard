import 'package:admin_dasboard/providers/auth_provider.dart';
import 'package:admin_dasboard/providers/register_from_provider.dart';
import 'package:admin_dasboard/router/route.dart';
import 'package:admin_dasboard/ui/buttons/custom_outlined_button.dart';
import 'package:admin_dasboard/ui/buttons/link_text.dart';
import 'package:admin_dasboard/ui/inpust/custom_inputs.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ReguisterView extends StatelessWidget {
  const ReguisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => RegisterFormProvider(),
      child: Builder(
        builder: (context) {
          final pro = Provider.of<RegisterFormProvider>(context, listen: false);
          return Container(
            color: Colors.black,
            margin: EdgeInsetsDirectional.only(top: 100),
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Center(
              child: ConstrainedBox(
                constraints: BoxConstraints(maxWidth: 370),
                child: Center(
                  child: Form(
                    key: pro.keyForom,
                    autovalidateMode: AutovalidateMode.always,
                    child: Column(
                      children: [
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty)
                              return 'iongresa usuario';
                            if (value.length < 6)
                              return 'El usuari debe de ser de 6 caracteres';
                            return null;
                          },
                          onChanged: (val) => pro.name = val,
                          style: TextStyle(color: Colors.white),
                          decoration: CustomInputs.loginInputDecorations(
                              hint: 'nombre',
                              icon: Icons.verified_user_outlined,
                              label: 'nombre'),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          validator: (val) {
                            if (!EmailValidator.validate(val ?? ""))
                              return 'correo ivalido';
                            return null;
                          },
                          onChanged: (val) => pro.emails = val,
                          style: TextStyle(color: Colors.white),
                          decoration: CustomInputs.loginInputDecorations(
                              hint: 'Coreo', icon: Icons.email, label: 'Coreo'),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty)
                              return 'iongresa contraseña';
                            if (value.length < 6)
                              return 'La contraseña debe de ser de 6 caracteres';
                            return null;
                          },
                          onChanged: (val) => pro.pass = val,
                          style: TextStyle(color: Colors.white),
                          obscureText: true,
                          decoration: CustomInputs.loginInputDecorations(
                              hint: '******',
                              icon: Icons.lock,
                              label: 'contraseña'),
                        ),
                        SizedBox(height: 20),
                        CustomOutlinedButton(
                            onpress: () {
                              final res = pro.validateForm();
                              if (!res) return;

                              final proAut = Provider.of<AuthProvider>(context,
                                  listen: false);
                              proAut.register(pro.name, pro.emails, pro.pass);
                            },
                            text: 'ingresar'),
                        SizedBox(height: 20),
                        LinkText(
                          text: 'Nueva Cuenta',
                          onPress: () {
                            Navigator.pushNamed(
                                context, Flurorouter.loginRoute);
                          },
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
