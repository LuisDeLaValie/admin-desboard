import 'package:admin_dasboard/ui/cards/while_card.dart';
import 'package:admin_dasboard/ui/labels/custom_labels.dart';
import 'package:flutter/material.dart';

class IconsView extends StatelessWidget {
  const IconsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Icons view',
              style: CustomLabels.h1,
            ),
            SizedBox(height: 10),
            Wrap(
              children: [
                WhileCard(
                  title: 'ac_unit_outlined',
                  child: Center(
                    child: Icon(
                      Icons.ac_unit_outlined,
                    ),
                  ),
                  width: 170,
                ),
                WhileCard(
                  title: 'accessible_forward_sharp',
                  child: Center(
                    child: Icon(
                      Icons.accessible_forward_sharp,
                    ),
                  ),
                  width: 170,
                ),
                WhileCard(
                  title: 'eco_rounded',
                  child: Center(
                    child: Icon(
                      Icons.eco_rounded,
                    ),
                  ),
                  width: 170,
                ),
                WhileCard(
                  title: 'access_time_outlined',
                  child: Center(
                    child: Icon(
                      Icons.access_time_outlined,
                    ),
                  ),
                  width: 170,
                ),
                WhileCard(
                  title: 'saved_search_sharp',
                  child: Center(
                    child: Icon(
                      Icons.saved_search_sharp,
                    ),
                  ),
                  width: 170,
                ),
                WhileCard(
                  title: 'one_x_mobiledata_rounded',
                  child: Center(
                    child: Icon(
                      Icons.one_x_mobiledata_rounded,
                    ),
                  ),
                  width: 170,
                ),
                WhileCard(
                  title: 'dashboard_sharp',
                  child: Center(
                    child: Icon(
                      Icons.dashboard_sharp,
                    ),
                  ),
                  width: 170,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
