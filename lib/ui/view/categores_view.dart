import 'package:admin_dasboard/dataDatble/categorias_datasources.dart';
import 'package:admin_dasboard/providers/categorias_provider.dart';
import 'package:admin_dasboard/ui/labels/custom_labels.dart';
import 'package:admin_dasboard/ui/modals/categoriModal.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoriView extends StatefulWidget {
  const CategoriView({Key? key}) : super(key: key);

  @override
  _CategoriViewState createState() => _CategoriViewState();
}

class _CategoriViewState extends State<CategoriView> {
  @override
  void initState() {
    super.initState();
    final pro = Provider.of<CategoriasProvider>(context, listen: false);

    pro.getCAtegorias();
  }

  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;

  @override
  Widget build(BuildContext context) {
    final categorias = Provider.of<CategoriasProvider>(context).categoria;

    return Container(
      child: ListView(
        physics: ClampingScrollPhysics(),
        children: [
          Text('Categorías', style: CustomLabels.h1),
          SizedBox(height: 10),
          PaginatedDataTable(
            columns: [
              DataColumn(label: Text('ID')),
              DataColumn(label: Text('Categoría')),
              DataColumn(label: Text('Creado por')),
              DataColumn(label: Text('Acciones')),
            ],
            source: CategorisDataTableSources(categorias, context),
            header: Text('Categorías disponibles', maxLines: 2),
            onRowsPerPageChanged: (value) {
              setState(() {
                _rowsPerPage = value ?? 10;
              });
            },
            rowsPerPage: _rowsPerPage,
            actions: [
              IconButton(
                onPressed: () {
                  showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      context: context,
                      builder: (_) => CategorisModal(categoria: null));
                },
                icon: Icon(Icons.add_outlined),
              ),

              // CustomIconButton(
              //   onPressed: () {
              //     showModalBottomSheet(
              //       backgroundColor: Colors.transparent,
              //       context: context,
              //       builder: ( _ ) => CategoryModal( categoria: null )
              //     );
              //   },
              //   text: 'Crear',
              //   icon: Icons.add_outlined,
              // )
            ],
          )
        ],
      ),
    );
  }
}
