import 'package:admin_dasboard/providers/auth_provider.dart';
import 'package:admin_dasboard/providers/login_form_provider.dart';
import 'package:admin_dasboard/router/route.dart';
import 'package:admin_dasboard/ui/buttons/custom_outlined_button.dart';
import 'package:admin_dasboard/ui/buttons/link_text.dart';
import 'package:admin_dasboard/ui/inpust/custom_inputs.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authPro = Provider.of<AuthProvider>(context);
    return ChangeNotifierProvider(
      create: (_) => LoginFormProvider(),
      child: Builder(
        builder: (context) {
          final pro = Provider.of<LoginFormProvider>(context, listen: false);

          return Container(
            color: Colors.black,
            margin: EdgeInsetsDirectional.only(top: 100),
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Center(
              child: ConstrainedBox(
                constraints: BoxConstraints(maxWidth: 370),
                child: Center(
                  child: Form(
                    key: pro.keyForom,
                    autovalidateMode: AutovalidateMode.always,
                    child: Column(
                      children: [
                        TextFormField(
                          onFieldSubmitted: (val) {
                            onSumite(pro, authPro);
                          },
                          validator: (val) {
                            if (!EmailValidator.validate(val ?? ""))
                              return 'correo ivalido';
                            return null;
                          },
                          onChanged: (val) => pro.emails = val,
                          style: TextStyle(color: Colors.white),
                          decoration: CustomInputs.loginInputDecorations(
                              hint: 'Coreo', icon: Icons.email, label: 'Coreo'),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          onFieldSubmitted: (val) {
                            onSumite(pro, authPro);
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty)
                              return 'iongresa contraseña';
                            if (value.length < 6)
                              return 'La contraseña debe de ser de 6 caracteres';
                            return null;
                          },
                          onChanged: (val) => pro.pass = val,
                          style: TextStyle(color: Colors.white),
                          obscureText: true,
                          decoration: CustomInputs.loginInputDecorations(
                              hint: '******',
                              icon: Icons.lock,
                              label: 'contraseña'),
                        ),
                        SizedBox(height: 20),
                        CustomOutlinedButton(
                          onpress: () {
                            onSumite(pro, authPro);
                          },
                          text: 'ingresar',
                        ),
                        SizedBox(height: 20),
                        LinkText(
                          text: 'Nueva Cuenta',
                          onPress: () {
                            Navigator.pushNamed(
                                context, Flurorouter.registerRoute);
                          },
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void onSumite(LoginFormProvider pro, AuthProvider authPro) {
    if (pro.validateForm()) {
      authPro.login(pro.emails, pro.pass);
    }
  }
}
