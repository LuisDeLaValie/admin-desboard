import 'package:admin_dasboard/dataDatble/user_dataSourse.dart';
import 'package:admin_dasboard/ui/labels/custom_labels.dart';
import 'package:flutter/material.dart';

class UsersView extends StatelessWidget {
  const UsersView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Useres view',
              style: CustomLabels.h1,
            ),
            SizedBox(height: 10),
            Container(
              width: double.infinity,
              child: PaginatedDataTable(
                columns: [
                  DataColumn(label: Text('Avatr')),
                  DataColumn(label: Text('Nombre')),
                  DataColumn(label: Text('Email')),
                  DataColumn(label: Text('UID')),
                  DataColumn(label: Text('Acciones')),
                ],
                source: UsersDataSorse(context),
                header: Text('Usuarios disponibles', maxLines: 2),
                // onRowsPerPageChanged: (value) {
                //   setState(() {
                //     _rowsPerPage = value ?? 10;
                //   });
                // },
                // rowsPerPage: _rowsPerPage,
                // actions: [
                //   IconButton(
                //     onPressed: () {
                //       showModalBottomSheet(
                //           backgroundColor: Colors.transparent,
                //           context: context,
                //           builder: (_) => CategorisModal(categoria: null));
                //     },
                //     icon: Icon(Icons.add_outlined),
                //   ),

                // ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
