import 'package:admin_dasboard/providers/sidemenu_provider.dart';
import 'package:admin_dasboard/ui/shared/widgets/NavbarAvatar.dart';
import 'package:admin_dasboard/ui/shared/widgets/NotificationIndicator.dart';
import 'package:admin_dasboard/ui/shared/widgets/searchText.dart';
import 'package:flutter/material.dart';

class Navbar extends StatelessWidget {
  const Navbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: 50,
      decoration: boxDecoration(),
      child: Row(
        children: [
          //todo: icon del menu
          if (size.width <= 700)
            IconButton(
                onPressed: () {
                  SideMenuProvider.onpenMenu();
                },
                icon: Icon(Icons.menu_outlined)),
          SizedBox(width: 5),

          if (size.width > 440)
            //share input
            ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 250),
              child: SearchText(),
            ),

          Spacer(),
          NotificationIndicator(),
          SizedBox(width: 10),
          NavbarAvatar(),
          SizedBox(width: 10),
        ],
      ),
    );
  }

  BoxDecoration boxDecoration() {
    return BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.black12,
          blurRadius: 10,
        )
      ],
    );
  }
}
