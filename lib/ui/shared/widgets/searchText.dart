import 'package:admin_dasboard/ui/inpust/custom_inputs.dart';
import 'package:flutter/material.dart';

class SearchText extends StatelessWidget {
  const SearchText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      color: Colors.grey.withOpacity(0.1),
      borderRadius: BorderRadius.circular(10),
    );
    return Container(
      height: 40,
      decoration: boxDecoration,
      child: TextField(
        decoration: CustomInputs.searchInputDecorations(hint: 'Buscar', icon: Icons.search_outlined),
      ),
    );
  }
}
