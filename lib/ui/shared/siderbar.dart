import 'package:admin_dasboard/providers/auth_provider.dart';
import 'package:admin_dasboard/providers/sidemenu_provider.dart';
import 'package:admin_dasboard/router/route.dart';
import 'package:admin_dasboard/services/navigarions_servises.dart';
import 'package:admin_dasboard/ui/shared/widgets/logo.dart';
import 'package:admin_dasboard/ui/shared/widgets/munu_item.dart';
import 'package:admin_dasboard/ui/shared/widgets/text_separator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Siderbar extends StatelessWidget {
  const Siderbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pro = Provider.of<SideMenuProvider>(context);

    final proAuth = Provider.of<AuthProvider>(context);
    return Container(
      width: 200,
      height: double.infinity,
      decoration: buildDecoration(),
      child: ListView(
        // para evitar que el escrol rebote
        physics: ClampingScrollPhysics(),
        children: [
          Logo(),
          SizedBox(height: 50),
          TextSeparator(text: 'main'),
          MenuItemm(
            isActive: pro.currentPage == Flurorouter.dashboardRoute,
            text: 'Dashboart',
            icon: Icons.compass_calibration_outlined,
            onPressed: () => navegar(Flurorouter.dashboardRoute),
          ),
          MenuItemm(
              text: 'Ordders',
              icon: Icons.shopping_cart_outlined,
              onPressed: () {}),
          MenuItemm(
              text: 'Analytic',
              icon: Icons.show_chart_outlined,
              onPressed: () {}),
          MenuItemm(
              text: 'CAtegories',
              icon: Icons.layers_outlined,
              isActive: pro.currentPage == Flurorouter.categoriasRoute,
              onPressed: () => navegar(Flurorouter.categoriasRoute)),
          MenuItemm(
              text: 'Products',
              icon: Icons.dashboard_outlined,
              onPressed: () {}),
          MenuItemm(
              text: 'Discount',
              icon: Icons.attach_money_outlined,
              onPressed: () {}),
          MenuItemm(
            text: 'Useres',
            icon: Icons.people_alt_rounded,
            isActive: pro.currentPage == Flurorouter.userRoute,
            onPressed: () => navegar(Flurorouter.userRoute),
          ),
          SizedBox(height: 30),
          TextSeparator(text: 'UI Element'),
          MenuItemm(
            isActive: pro.currentPage == Flurorouter.iconsRoute,
            text: 'Icons',
            icon: Icons.list_alt_outlined,
            onPressed: () => navegar(Flurorouter.iconsRoute),
          ),
          MenuItemm(
              text: 'Marketing',
              icon: Icons.mark_email_read_outlined,
              onPressed: () {}),
          MenuItemm(
              text: 'Cmpaing', icon: Icons.note_add_outlined, onPressed: () {}),
          MenuItemm(
              text: 'Blank', icon: Icons.note_add_outlined, onPressed: () {}),
          SizedBox(height: 50),
          TextSeparator(text: 'Exit'),
          MenuItemm(
              text: 'Logout',
              icon: Icons.exit_to_app_outlined,
              onPressed: () {
                proAuth.logout();
              }),
        ],
      ),
    );
  }

  void navegar(String name) {
    SideMenuProvider.closeMenu();
    NavigationServices.replaceTo(name);
  }

  BoxDecoration buildDecoration() {
    return BoxDecoration(
      gradient: LinearGradient(
        colors: [
          Color(0xff092044),
          Color(0xff092042),
        ],
      ),
      boxShadow: [
        BoxShadow(color: Colors.black26, blurRadius: 10),
      ],
    );
  }
}
