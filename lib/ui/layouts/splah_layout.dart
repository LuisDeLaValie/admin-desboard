import 'package:flutter/material.dart';

class SpalsLayout extends StatelessWidget {
  const SpalsLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            CircularProgressIndicator(),
            SizedBox(
              height: 20,
            ),
            Text('Checking...'),
          ],
        ),
      ),
    );
  }
}
