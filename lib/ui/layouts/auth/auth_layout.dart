import 'package:admin_dasboard/ui/layouts/auth/widgets/links_bar.dart';
import 'package:flutter/material.dart';

import 'package:admin_dasboard/ui/layouts/auth/widgets/background_twitter.dart';
import 'package:admin_dasboard/ui/layouts/auth/widgets/custom_title.dart';

class AuthLayout extends StatelessWidget {
  final Widget child;
  const AuthLayout({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Scrollbar(
        // isAlwaysShown: true,
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: [
            (size.width > 1000)
                ?
                //Desktop
                _DesktopBody(child: this.child)
                :
                // mobile
                _MovileBody(child: child),
            //linkBar
            LinksBar()
          ],
        ),
      ),
    );
  }
}

class _DesktopBody extends StatelessWidget {
  final Widget child;

  const _DesktopBody({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.95,
      width: size.width,
      color: Colors.black,
      child: Row(
        children: [
          Expanded(
            child: Container(
              color: Colors.blue,
              child: BackgroundTwitter(),
            ),
          ),
          Container(
            width: 540,
            height: double.infinity,
            color: Colors.black,
            child: Column(children: [
              CustomTitle(),
              SizedBox(height: 50),
              Expanded(child: this.child),
            ]),
          ),
        ],
      ),
    );
  }
}

class _MovileBody extends StatelessWidget {
  final Widget child;

  const _MovileBody({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      // height: size.height * 0.95,
      width: size.width,
      color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          CustomTitle(),
          Container(
            height: 420,
            child: child,
          ),
          Container(
            width: double.infinity,
            height: 400,
            child: BackgroundTwitter(),
          ),
        ],
      ),
    );
  }
}
