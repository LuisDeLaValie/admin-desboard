import 'package:admin_dasboard/ui/buttons/link_text.dart';
import 'package:flutter/material.dart';

class LinksBar extends StatelessWidget {
  const LinksBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: (size.width > 1000) ? size.height * 0.1 : null,
        color: Colors.black,
        child: Wrap(
          alignment: WrapAlignment.center,
          children: [
            LinkText(text: 'About'),
            LinkText(text: 'Help Senter'),
            LinkText(text: 'Terms of Services'),
            LinkText(text: 'privacy polityque'),
            LinkText(text: 'cookis polity'),
            LinkText(text: 'ads info'),
            LinkText(text: 'blog'),
            LinkText(text: 'status'),
            LinkText(text: 'CAreers'),
            LinkText(text: 'Barnd Resoruses'),
            LinkText(text: 'Markenting'),
          ],
        ));
  }
}
