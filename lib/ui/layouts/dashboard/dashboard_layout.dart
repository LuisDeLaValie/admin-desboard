import 'package:admin_dasboard/providers/sidemenu_provider.dart';
import 'package:admin_dasboard/ui/shared/navbar.dart';
import 'package:admin_dasboard/ui/shared/siderbar.dart';
import 'package:flutter/material.dart';

class DashboardLayout extends StatefulWidget {
  final Widget child;
  DashboardLayout({Key? key, required this.child}) : super(key: key);

  @override
  _DashboardLayoutState createState() => _DashboardLayoutState();
}

class _DashboardLayoutState extends State<DashboardLayout>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    SideMenuProvider.menuController = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 300));
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xffEDF1F2),
      body: Stack(
        children: [
          Row(
            children: [
              //todo: esto si es mas de 700 pixels
              if (size.width > 700) Siderbar(),

              Expanded(
                child: Column(
                  children: [
                    //navar
                    Navbar(),

                    /// contenedor del view
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      child: widget.child,
                    )),
                  ],
                ),
              )
            ],
          ),
          if (size.width <= 700)
            AnimatedBuilder(
              animation: SideMenuProvider.menuController,
              child: widget.child,
              builder: (context, _) {
                return Stack(
                  children: [
                    //todo: vaggruond del sidervar
                    if (SideMenuProvider.isOpen)
                      Opacity(
                        opacity: SideMenuProvider.opacity.value,
                        child: GestureDetector(
                          onTap: () => SideMenuProvider.closeMenu(),
                          child: Container(
                            width: size.width,
                            height: size.height,
                            color: Colors.black26,
                          ),
                        ),
                      ),

                    Transform.translate(
                      offset: Offset(SideMenuProvider.movement.value, 0),
                      child: Siderbar(),
                    ),
                  ],
                );
              },
            ),
        ],
      ),
    );
  }
}
