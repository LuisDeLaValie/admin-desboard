
import 'package:admin_dasboard/providers/categorias_provider.dart';
import 'package:admin_dasboard/services/notification_services.dart';
import 'package:admin_dasboard/ui/inpust/custom_inputs.dart';
import 'package:admin_dasboard/ui/labels/custom_labels.dart';
import 'package:flutter/material.dart';

import 'package:admin_dasboard/models/categorias.dart';
import 'package:provider/provider.dart';

class CategorisModal extends StatefulWidget {
  final Categoria? categoria;
  const CategorisModal({Key? key, this.categoria}) : super(key: key);

  @override
  _CategorisModalState createState() => _CategorisModalState();
}

class _CategorisModalState extends State<CategorisModal> {
  String nombre = "";
  String? id;

  @override
  void initState() {
    super.initState();
    id = widget.categoria?.id;
    nombre = widget.categoria?.nombre ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      padding: EdgeInsets.all(20),
      decoration: newMethod(),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                (id == null) ? "Nueva CAtegoria" : "Editar CAtegoria",
                style: CustomLabels.h1.copyWith(color: Colors.white),
              ),
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          Divider(color: Colors.white.withOpacity(0.3)),
          SizedBox(height: 20),
          TextFormField(
              initialValue: nombre,
              onChanged: (val) => nombre = val,
              decoration: CustomInputs.loginInputDecorations(
                hint: 'Nombre de la CAtegoria',
                label: 'CAtegoria',
                icon: Icons.new_releases_outlined,
              ),
              style: TextStyle(color: Colors.white)),
          SizedBox(height: 20),
          ElevatedButton.icon(
            onPressed: () async {
              final pro =
                  Provider.of<CategoriasProvider>(context, listen: false);

              if (id == null) {
                await pro.nuevaCAtegoria(nombre);
                NotificationServices.showSnackBar('Creado');
              } else {
                await pro.actulizarCAtegoria(id!, nombre);
                NotificationServices.showSnackBar('Actualizado');
              }

              Navigator.pop(context);
            },
            icon: Icon(Icons.save_outlined),
            label: Text('Guardar'),
          ),
        ],
      ),
    );
  }

  BoxDecoration newMethod() => BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        color: Color(0xff0F2041),
        boxShadow: [
          BoxShadow(color: Colors.black26),
        ],
      );
}
