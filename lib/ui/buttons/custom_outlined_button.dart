import 'package:flutter/material.dart';

class CustomOutlinedButton extends StatelessWidget {
  final Function onpress;
  final String text;
  final Color color;
  final bool isFillde;
  const CustomOutlinedButton({
    Key? key,
    required this.onpress,
    required this.text,
    this.color = Colors.blue,
    this.isFillde = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: ButtonStyle(
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
          ),
          side: MaterialStateProperty.all(
            BorderSide(color: color),
          ),
          backgroundColor: MaterialStateProperty.all(
            isFillde ? color.withOpacity(0.3) : Colors.transparent,
          )),
      onPressed: () => onpress(),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Text(
          text,
          style: TextStyle(fontSize: 16),
        ),
      ),
    );
  }
}
