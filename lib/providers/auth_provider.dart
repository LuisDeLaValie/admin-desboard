import 'package:admin_dasboard/api/cafeApi.dart';
import 'package:admin_dasboard/models/http/auth_response.dart';
import 'package:admin_dasboard/models/usuario.dart';
import 'package:admin_dasboard/router/route.dart';
import 'package:admin_dasboard/services/loaclStorach.dart';
import 'package:admin_dasboard/services/navigarions_servises.dart';
import 'package:admin_dasboard/services/notification_services.dart';
import 'package:flutter/material.dart';

enum AuthSatate {
  cehking,
  authenticated,
  notAuthenticated,
}

class AuthProvider with ChangeNotifier {
  Usuario? user;

  AuthSatate status = AuthSatate.cehking;

  AuthProvider() {
    this.isAuththenticated();
  }

  void login(String name, String pass) async {
    final data = {
      'correo': name,
      'password': pass,
    };

    CafeApi.httpPost('/api/auth/login', data).then((value) {
      

      final authResponse = AuthResponse.fromMap(value);

      this.user = authResponse.usuario;

      status = AuthSatate.authenticated;
      LocalStorage.pref.setString('token', authResponse.token);

      CafeApi.configureDio();
      NavigationServices.replaceTo(Flurorouter.dashboardRoute);
      notifyListeners();
    }).catchError((e) {
      print("error en $e");
      //todo: mostrar notificacion

      NotificationServices.showSnackBarErro(e.toString());
    });
  }

  void register(String name, String email, String pass) async {
    final data = {
      'nombre': name,
      'correo': email,
      'password': pass,
    };

    CafeApi.httpPost('/api/usuarios', data).then((value) {

      final authResponse = AuthResponse.fromMap(value);

      this.user = authResponse.usuario;

      status = AuthSatate.authenticated;
      LocalStorage.pref.setString('token', authResponse.token);

      CafeApi.configureDio();

      NavigationServices.replaceTo(Flurorouter.dashboardRoute);

      notifyListeners();
    }).catchError((e) {
      //todo: mostrar notificacion

      NotificationServices.showSnackBarErro(e.toString());
    });
  }

  Future<bool> isAuththenticated() async {
    final token = LocalStorage.pref.getString('token');
    if (token == null) {
      status = AuthSatate.notAuthenticated;
      notifyListeners();
      return false;
    }

    //Todo: comprobar el jwt

    CafeApi.httpGet('/api/auth').then((value) {

      final authResponse = AuthResponse.fromMap(value);

      this.user = authResponse.usuario;
      status = AuthSatate.authenticated;
      LocalStorage.pref.setString('token', authResponse.token);

      CafeApi.configureDio();

      notifyListeners();
      return true;

      // NavigationServices.replaceTo(Flurorouter.dashboardRoute);
    }).catchError((e) {
      print("error en $e");
      //todo: mostrar notificacion

      NotificationServices.showSnackBarErro(e.toString());
      return false;
    });

    return true;
  }

  void logout() {
    LocalStorage.pref.remove('token');
    status = AuthSatate.notAuthenticated;
    notifyListeners();
  }
}
