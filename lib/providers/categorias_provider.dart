import 'package:admin_dasboard/api/cafeApi.dart';
import 'package:admin_dasboard/models/categorias.dart';
import 'package:admin_dasboard/models/http/categorisResponse.dart';
import 'package:flutter/material.dart';

class CategoriasProvider with ChangeNotifier {
  List<Categoria> categoria = [];

  Future<void> getCAtegorias() async {
    final res = await CafeApi.httpGet('/api/categorias');
    final categorias = CategorisResponse.fromMap(res);
    this.categoria = categorias.categorias;

    notifyListeners();
  }

  Future<void> nuevaCAtegoria(String nombre) async {
    final data = {
      "nombre": nombre,
    };

    try {
      final res = await CafeApi.httpPost('/api/categorias', data);
      final categoria = Categoria.fromMap(res);
      this.categoria.add(categoria);

      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> actulizarCAtegoria(String id, String nombre) async {
    final data = {
      "nombre": nombre,
    };

    try {
      await CafeApi.httpPut('/api/categorias/$id', data);

      this.categoria = this.categoria.map((e) {
        if (e.id != id) return e;

        return e.copyWith(nombre: nombre);
      }).toList();

      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> deletCAtegoria(String id) async {
    try {
      await CafeApi.httpDel('/api/categorias/$id');
      this.categoria.removeWhere((e) => e.id == id);

      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}
