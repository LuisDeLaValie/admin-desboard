import 'package:flutter/material.dart';

class SideMenuProvider with ChangeNotifier {
  static late AnimationController menuController;
  static bool isOpen = false;

  static Animation<double> movement = Tween<double>(begin: -200, end: 0)
      .animate(
          CurvedAnimation(parent: menuController, curve: Curves.easeInOut));

  static Animation<double> opacity = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(parent: menuController, curve: Curves.easeInOut));

  static void onpenMenu() {
    isOpen = true;
    menuController.forward();
  }

  static void closeMenu() {
    isOpen = false;
    menuController.reverse();
  }

  String _currentPage = "";
  String get currentPage => this._currentPage;
  setcurrentPage(String val) {
    this._currentPage = val;
    Future.delayed(Duration(milliseconds: 100)).then((value) {
      notifyListeners();
    });
  }

  void init() async {
    notifyListeners();
  }
}
