import 'package:flutter/material.dart';

class LoginFormProvider with ChangeNotifier {
  final keyForom = GlobalKey<FormState>();

  String emails = '';
  String pass = '';

  bool validateForm() {
    return this.keyForom.currentState!.validate();
    
  }
}
