import 'package:flutter/material.dart';

class RegisterFormProvider with ChangeNotifier {
  final keyForom = GlobalKey<FormState>();

  String emails = '';
  String name = '';
  String pass = '';

  bool validateForm() {
    return this.keyForom.currentState!.validate();
  }
}
